# Devops

## Name
Devops Deploy

## Description

Repository was created in order to deploy the fake Application using some pipelines rules \
The project is consuming some yamls from the central pipelines repository (https://gitlab.com/hyanfelipesc/pipeline)


## Pipelines

The pipelines were created following the instructions as you can see on the image bellow:

![Variable image](img/pipelinesrunning.png)

Dev:

![Variable image](img/devHyan.png)

Production:

![Variable image](img/prodHyan.png)

Other Branchs:

![Variable image](img/otherHyan.png)


## Script Slack
The script to send a message to the slack channel was created (just a fake script so far), it is stored on the pipeline repository and called using a yaml and gitlab API (To download only the script)

![Variable image](img/gitlab-api.png)

A personal token was created (Project token is a premium feature) as well as a variable to store its value.

![Variable image](img/variables.png)
